import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const registerUser = new RegisterController();
const currentUser = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Register controller`, () => {

    let accessToken: string;
    let userName: string;     
        userName =  "LeraQABSA";

    it(`should return 200 status code and token`, async () => {    
        let response = await registerUser.register(0, "string", "leraQA.bsa@gmail.com", userName, "password" );
    
        checkStatusCode(response, 201);
        checkResponseTime(response,1000);
        expect(response.body,`Response jsonSchema is correct`).to.be.jsonSchema(schemas.schema_registerUser);
        expect(response.body.user.userName, `userName` ).to.equal(userName);

        accessToken = response.body.token.accessToken.token;
        console.log(response.body);
        console.log(accessToken);        

    });

    it(`the username in the getCurrentUser function is the same as it was specified at registration`, async () => {  
        let response = await currentUser.getCurrentUser(accessToken);
        console.log(response.body);

        expect(response.body.userName, `userName at getCurrentUser function is the same at registration` ).to.equal(userName);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);   
    });

});
