import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const currentUser = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Delete user controller`, () => {
    let userID: number;
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("NEW@gmail.com", "password");

        accessToken = response.body.token.accessToken.token;
        userID = response.body.user.id;
        console.log(userID);
    });

    it(`should return 204 status code`, async () => {  
        let response = await currentUser.deleteUserById(userID, accessToken);
    
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);    
    });

    it(`should return 400 status code when delete user with invalidUserID`, async () => {  
        userID = 999999998888888;
        let response = await currentUser.deleteUserById(userID, accessToken);   
        checkStatusCode(response, 400);
        checkResponseTime(response,1000);  
    });
});
