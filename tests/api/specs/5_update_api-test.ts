import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;
    let userID: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login("leraQA.bsa@gmail.com", "password");

        accessToken = response.body.token.accessToken.token;
        console.log(accessToken);
        userID = response.body.user.id;
    });

    it(`Update user`, async () => {
        let userData: object = {
            id: userID,
            avatar: "string",
            email: "NEW@gmail.com",
            userName: "NEWLeraQa",
        };

        let response = await users.updateUser(userData, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });
});

