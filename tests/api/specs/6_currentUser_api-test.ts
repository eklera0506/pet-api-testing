import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const currentUser = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Current user controller`, () => {
    let accessToken: string;


    before(`Login and get the token`, async () => {
        let response = await auth.login("NEW@gmail.com", "password");

        accessToken = response.body.token.accessToken.token;
         console.log(accessToken);
    });

    it(`should return 200 status code`, async () => {  
        let response = await currentUser.getCurrentUser(accessToken);
        console.log(response.body);
    
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body,`Response json schema is correct`).to.be.jsonSchema(schemas.schema_userByID);
    });
});
