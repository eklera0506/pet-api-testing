import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const post = new PostsController();
const auth = new AuthController();

describe("Create post controller", () => {
    let accessToken: string;
    let userID: number;
    let postID: number;
    

    before(`Login and get the token`, async () => {
        let response = await auth.login("NEW@gmail.com", "password");

        accessToken = response.body.token.accessToken.token;
        userID = response.body.user.id;
        console.log(accessToken);
    });

    it(`Create post`, async () => {
        let postData: object = {
            authorId: userID,
            previewImage: "MyImage",
            body: "MyGoodPost"          
        };

        let response = await post.createPost(postData, accessToken);
        postID = response.body.id;
        console.log(response.body);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`Put like`, async () => {
        let postData: object = {
            entityId: postID,
            isLike: true,
            userId: userID    
        };

        let response = await post.putLikePost(postData, accessToken);
        console.log(response.body);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);

    });

    it(`Comment post`, async () => {
        let postData: object = {
            authorId: userID,
            postId: postID,
            body: "It's the best post I've ever seen"    
        };

        let response = await post.commentsPost(postData, accessToken);
        console.log(response.body);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);

    });

    it(`Comment post with invalid PostID`, async () => {
        let postData: object = {
            authorId: userID,
            postId: 6767676767676,
            body: "It's the best post I've ever seen"    
        };

        let response = await post.commentsPost(postData, accessToken);
        checkStatusCode(response, 400);
        checkResponseTime(response,1000);
    });
});
