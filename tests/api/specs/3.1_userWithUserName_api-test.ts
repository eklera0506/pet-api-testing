import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
    let userName: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("leraQA.bsa@gmail.com", "password");

        userName = response.body.user.userName;
    });
  
    it(`Response includes user with userName`, async () => {
        let response = await users.getAllUsers();
        var expectedUserName = userName;
        var responseBody = response.body;
    
        var usersWithExpectedUserName = responseBody.filter(function(user) {
            return user.userName == expectedUserName;
        });
    
        expect(usersWithExpectedUserName.length, `User is find`).to.be.above(0);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);     
    }); 
});
