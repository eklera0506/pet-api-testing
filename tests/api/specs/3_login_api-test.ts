import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const loginUser = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Auth controller`, () => {

    it(`should return 200 status code and login`, async () => {
        let accessToken: string;      
        let response = await loginUser.login("leraQA.bsa@gmail.com", "password" );
    
        accessToken = response.body.token.accessToken.token;  
        console.log(response.body);   
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_authUser)      
    });
});
