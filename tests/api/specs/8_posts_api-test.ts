import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();
const chai = require('chai');

describe(`Posts controller`, () => {
 
    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await posts.getAllPosts();

        console.log("All Posts:");
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });
});
